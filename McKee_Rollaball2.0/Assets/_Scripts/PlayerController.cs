﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public float speed;
    public float jumppower;
    bool onGround;
    public Text countText;
    public Text winText;

    private Rigidbody rb;
    private int count;

  


    // Start is called before the first frame update

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText();
        winText.text = "";


     
    }

    private void Update()
    {
        //When Space is pressed, the player jumps
        onGround = Physics.Raycast(transform.position, Vector3.down, 1.0f);

        if (Input.GetKeyDown(KeyCode.Space) && onGround)

            Jump();
    }
   
    private void Jump()
    {
    // Creates Power for the Jump action
        rb.AddForce(Vector3.up * jumppower);
    }


    // Update is called once per frame
    void FixedUpdate()
    {
    //Movement for the Player
    float moveHorizontal = Input.GetAxis("Horizontal");
    float moveVertical = Input.GetAxis("Vertical");

    Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
        rb.AddForce(movement * speed);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pick Up"))
        {
            // adds to the counter of the amount of items picked up by the player.
            other.gameObject.SetActive(false);
            count = count + 1;
            SetCountText ();
//Changes size of player when colliding with a pickup (Decreases Size)
            transform.localScale += new Vector3 (-.1f, -.1f, -.1f);
        }
    }
    void SetCountText ()
    {
        //Makes wintext appear when all pickups have been collected
        countText.text = "Score:" + count.ToString();
        if (count >= 22)
        {
            print("victory");
            winText.text = "You Win!";
        }
    }
}
